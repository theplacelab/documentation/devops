const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("/Users/andrew/Code/Documentation/devops/.docz/.cache/dev-404-page.js"))),
  "component---content-index-md": hot(preferDefault(require("/Users/andrew/Code/Documentation/devops/content/index.md"))),
  "component---readme-md": hot(preferDefault(require("/Users/andrew/Code/Documentation/devops/README.md"))),
  "component---src-pages-404-js": hot(preferDefault(require("/Users/andrew/Code/Documentation/devops/.docz/src/pages/404.js")))
}

