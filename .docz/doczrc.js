/*
export default {
  menu: [
    'Welcome',
    { name: '📗User Guide', menu: ['Welcome', 'POI', 'Preferences'] },
    '🔧Developer Guide'
  ],
  files: '**\/*.{md,mdx}',
  port: 4000
};
*/

export default {
  menu: [],
  files: "**/*.{md,mdx}",
  port: 4000,
};
