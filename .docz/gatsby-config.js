const { mergeWith } = require('docz-utils')
const fs = require('fs-extra')

let custom = {}
const hasGatsbyConfig = fs.existsSync('./gatsby-config.custom.js')

if (hasGatsbyConfig) {
  try {
    custom = require('./gatsby-config.custom')
  } catch (err) {
    console.error(
      `Failed to load your gatsby-config.js file : `,
      JSON.stringify(err),
    )
  }
}

const config = {
  pathPrefix: '/',

  siteMetadata: {
    title: 'Feral Dev Ops',
    description: 'My awesome app using docz',
  },
  plugins: [
    {
      resolve: 'gatsby-theme-docz',
      options: {
        themeConfig: {},
        src: './',
        gatsbyRoot: null,
        themesDir: 'src',
        mdxExtensions: ['.md', '.mdx'],
        docgenConfig: {},
        menu: [],
        mdPlugins: [],
        hastPlugins: [],
        ignore: [],
        typescript: false,
        ts: false,
        propsParser: true,
        'props-parser': true,
        debug: false,
        native: false,
        openBrowser: null,
        o: null,
        open: null,
        'open-browser': null,
        root: '/Users/andrew/Code/Documentation/devops/.docz',
        base: '/',
        source: './',
        'gatsby-root': null,
        files: '**/*.{md,markdown,mdx}',
        public: '/public',
        dest: '.docz/dist',
        d: '.docz/dist',
        editBranch: 'master',
        eb: 'master',
        'edit-branch': 'master',
        config: '',
        title: 'Feral Dev Ops',
        description: 'My awesome app using docz',
        host: 'localhost',
        port: 3000,
        p: 3000,
        separator: '-',
        paths: {
          root: '/Users/andrew/Code/Documentation/devops',
          templates:
            '/Users/andrew/Code/Documentation/devops/node_modules/docz-core/dist/templates',
          docz: '/Users/andrew/Code/Documentation/devops/.docz',
          cache: '/Users/andrew/Code/Documentation/devops/.docz/.cache',
          app: '/Users/andrew/Code/Documentation/devops/.docz/app',
          appPackageJson:
            '/Users/andrew/Code/Documentation/devops/package.json',
          appTsConfig: '/Users/andrew/Code/Documentation/devops/tsconfig.json',
          gatsbyConfig:
            '/Users/andrew/Code/Documentation/devops/gatsby-config.js',
          gatsbyBrowser:
            '/Users/andrew/Code/Documentation/devops/gatsby-browser.js',
          gatsbyNode: '/Users/andrew/Code/Documentation/devops/gatsby-node.js',
          gatsbySSR: '/Users/andrew/Code/Documentation/devops/gatsby-ssr.js',
          importsJs:
            '/Users/andrew/Code/Documentation/devops/.docz/app/imports.js',
          rootJs: '/Users/andrew/Code/Documentation/devops/.docz/app/root.jsx',
          indexJs:
            '/Users/andrew/Code/Documentation/devops/.docz/app/index.jsx',
          indexHtml:
            '/Users/andrew/Code/Documentation/devops/.docz/app/index.html',
          db: '/Users/andrew/Code/Documentation/devops/.docz/app/db.json',
        },
      },
    },
  ],
}

const merge = mergeWith((objValue, srcValue) => {
  if (Array.isArray(objValue)) {
    return objValue.concat(srcValue)
  }
})

module.exports = merge(config, custom)
